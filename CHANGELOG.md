# Changelog

## 0.2.0

- Upgrade Snowflake Python Driver (`snowflake-connector-python`) to `2.9.0`
## 0.1.3

- [#31](https://gitlab.com/meltano/target-snowflake/-/issues/31) Store objects and arrays as JSON strings instead of stringified Python dict/list


