.PHONY: test test-quick test-run build mypy

APP_NAME=target_gitlab_snowflake
CHECKING_DIR=.

build:
	@echo "Building app..."
	@time docker build -t ${APP_NAME} .

test-full:
	@echo "Running full test suite..."
	@time docker run \
					-e SF_ACCOUNT -e SF_USER -e SF_PASSWORD -e SF_ROLE \
					-e SF_DATABASE -e SF_TEST_SCHEMA -e SF_WAREHOUSE \
					${APP_NAME} /bin/bash -c \
						"python target_snowflake/utils/config_generator.py ; \
						pytest tests/ -vv --config config.json"

test-quick:
	@echo 'Running tests with the "not slow" option on...'
	@time docker run \
					-e SF_ACCOUNT -e SF_USER -e SF_PASSWORD -e SF_ROLE \
					-e SF_DATABASE -e SF_TEST_SCHEMA -e SF_WAREHOUSE \
					${APP_NAME} /bin/bash -c \
						'python target_snowflake/utils/config_generator.py ; \
						pytest tests/ -vv --config config.json -m "not slow" '

test-run:
	@echo "Running target-snowflake with test data..."
	@time docker run \
					-e SF_ACCOUNT -e SF_USER -e SF_PASSWORD -e SF_ROLE \
					-e SF_DATABASE -e SF_TEST_SCHEMA -e SF_WAREHOUSE \
					${APP_NAME}  /bin/bash -c \
							"python target_snowflake/utils/config_generator.py ; \
							cat tests/data_files/user_location_data.stream | target-snowflake -c config.json ; \
							cat tests/data_files/array_data.stream | target-snowflake -c config.json"

#mypy:
#	@echo "Running mypy for type-checking..."
#	@time docker run ${APP_NAME} mypy target_snowflake --ignore-missing-imports
#
#lint:
#	@echo "Running linter..."
#	@time black target_snowflake tests

black:
	@echo "Running lint (black)..."
	@poetry run black "$(CHECKING_DIR)"

flake8:
	@echo "Running flake8..."
	@poetry run flake8 "$(CHECKING_DIR)" --ignore=E203,E501,W503,W605

mypy:
	@echo "Running mypy..."
	@poetry run mypy "$(CHECKING_DIR)" --ignore-missing-imports

pylint:
	@echo "Running pylint..."
	@poetry run pylint "$(CHECKING_DIR)/target_snowflake/" --disable=line-too-long,E0401,E0611,W1203,W1202
	@poetry run pylint "$(CHECKING_DIR)/tests/" --disable=line-too-long,E0401,E0611,W1203,W1202

vulture:
	@echo "Running vulture..."
	@poetry run vulture "$(CHECKING_DIR)" --min-confidence 100

isort:
	@echo "Running isort..."
	@poetry run isort .

pytest:
	@echo "Running pytest..."
	@poetry run python3 -m pytest -vv -x

python-code-quality: isort black flake8 vulture mypy pylint
	@echo "Running python-code-quality..."
